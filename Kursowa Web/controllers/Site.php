<?php


namespace controllers;


use core\Controller;

class Site extends Controller
{
    public function actionIndex()
    {
        $result = [
            'Title' => 'Заголовок сторінки',
            'Content' => 'Контент'
        ];
        return $this->render('index', null,[
            'MainTitle' => 'Дизайн-Проект',
            'PageTitle' => ''
        ]);
    }
}