<?php

namespace controllers;

use core\Controller;

class Users extends Controller
{
    protected $usersModel;
    function __construct()
    {
        $this->usersModel = new \models\Users();
    }
    function actionLogout()
    {
        $title = 'Вихід  з акаунту';
        unset($_SESSION['user']);
        return $this->renderMessage('ok','Ви вийшли з Вашого Акаунту',null,
            [
                'PageTitle' => $title,
                'MainTitle' => $title,
            ]);
    }
    function actionLogin()
    {
        $title = 'Вхід  на сайт';
        if (isset($_SESSION['user']))
            return $this->renderMessage('ok','Ви вже увійшли на сайт',null,
                [
                    'PageTitle' => $title,
                    'MainTitle' => $title,
                ]);
        if($this->isPost())
        {
            $user = $this->usersModel->AuthUser($_POST['login'],$_POST['password']);
            if(!empty($user))
            {
                $_SESSION['user'] = $user;
                return $this->renderMessage('ok','Ви успішно увійшли на сайт',null,
                [
                    'PageTitle' => $title,
                    'MainTitle' => $title,
                ]);
            } else// CORRECT 10
                return $this->render('login',null, [
                    'PageTitle' => $title,
                    'MainTitle' => $title,
                    'MessageText' => 'Неправильний логін або пароль',
                    'MessageClass' => 'danger',
                ]);
        }else {
            $params = [
                'PageTitle' => $title,
                'MainTitle' => $title,
            ];
            return $this->render('login',null, $params);
        }
    }// CORRECT 10
    function actionRegister()
    {
        if($this->isPost())
        {
            $result = $this->usersModel->AddUser($_POST);
            if($result === true){
                return $this->renderMessage('ok','Користувач успішно зареєстрований',null,
                [
                    'PageTitle' => 'Pеєстрація на сайт',
                    'MainTitle' => 'Pеєстрація на сайт',
            ]);
            }else
            {
                $message = implode('<br/> ',$result);
                return $this->render('register',null, [
                    'PageTitle' => 'Pеєстрація на сайт',
                    'MainTitle' => 'Pеєстрація на сайт',
                    'MessageText' => $message,
                    'MessageClass' => 'danger',
                ]);
            }
        }else {
            $params = [
                'PageTitle' => 'Pеєстрація на сайт',
                'MainTitle' => 'Pеєстрація на сайт',
            ];
        return $this->render('register',null, $params);
        }
    }
}