<?php
$userModel = new  \models\Users();
$user = $userModel->GetCurrentUser();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title><?=$MainTitle ?></title>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="/style.css" type="text/css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
    <script type="module">
        import { Fancybox } from "https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.esm.js";
    </script>
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css"
    />
</head>
    <link href="https://fonts.googleapis.com/css?family=DM+Sans:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/aos.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">

</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">


<div class="site-wrap" id="home-section">

    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar site-navbar-target bg-white" role="banner">


            <div class="col-lg-12">
                <form class="d-flex">
                    <? if(!$userModel->IsUserAuthenticated()) : ?>
                        <a href="/users/register" class="btn btn-outline-primary">Реєстрація</a>
                        <a href="/users/login" class="btn btn-primary">Увійти</a>
                    <? else: ?>
                        <span class="nav-link"><?=$user ['login'] ?></span>
                        <a href="/users/logout" class="btn btn-primary">Вийти</a>
                    <? endif; ?>
                </form>
                <div class= "container">
                    <? if(!empty($MessageText)):?>
                        <div class="alert alert-<?=$MessageClass ?>" role="alert">
                            <?=$MessageText ?>
                        </div>
                    <? endif; ?>
                    <? ?>
                    <?=$PageContent ?>
                </div>

        <div class="container">
            <div class="row align-items-center position-relative">

                <div class="col-lg-4">
                    <nav class="site-navigation text-right ml-auto " role="navigation">
                        <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">

                            <li class="nav-item"><a class="nav-link" href="/index.php">Головна </a></li>
                            <li class="nav-item"><a class="nav-link" href="/news">Новини</a></li>
                            <li><a href="project.html" class="nav-link">Проекти</a></li>
                            <div class="container">
                    </nav>
                </div>

                <div class="col-lg-4 text-center">
                    <div class="site-logo">
                        <a href="/index.php">ДИЗАЙН-ПРОЕКТ</a>
                    </div>


                    <div class="ml-auto toggle-button d-inline-block d-lg-none"><a href="/" class="site-menu-toggle py-5 js-menu-toggle text-white"><span class="icon-menu h3 text-primary"></span></a></div>
                </div>
                <div class="col-lg-4">
                    <nav class="site-navigation text-left mr-auto " role="navigation">
                        <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                            <li><a href="about.html" class="nav-link">Про нас</a></li>
                            <li><a href="blog.html" class="nav-link">Блог</a></li>
                            <li><a href="contact.html" class="nav-link">Контакти</a></li>
                        </ul>
                    </nav>
                </div>


    <div class="owl-carousel-wrapper">



        <div class="box-92819">
            <div class="owl-carousel slide-one-item-alt-text">

                <div>
                    <h1 class="text-uppercase mb-3">Ми спеціалісти в галузі Архітектори та Дизайну</h1>
                    <p class="mb-5">Витончений вишуканий інтер'єр створений для молодої сімейної пари, що захоплюється мистецтвом та сучасним дизайном.</p>
                    <p class="mb-0"><a href="/contact.html" class="btn btn-primary rounded-0">Наші контакти</a></p>
                </div>

                <div>
                    <h1 class="text-uppercase mb-3">Дизайн, завдяки якому ви почуваєте себе як вдома</h1>
                    <p class="mb-5">Індустріальний стиль інтер'єру для молодої пари. Маленьку площу об'єднали в опенспейс, де спальня та ванна укладені у стильний квадратний бокс.</p>
                    <p class="mb-0"><a href="/contact.html" class="btn btn-primary rounded-0">Наші контакти</a></p>
                </div>

                <div>
                    <h1 class="text-uppercase mb-3">Готові почати втілювати вашу мрію</h1>
                    <p class="mb-5">При оформленні невеликої затишної квартири пріоритетним завданням було змінити початкове планувальне рішення,
                        щоб позбутися непотрібних перегородок та створити грамотне та функціональне приміщення, що вміщає все необхідне для комфортного життя.</p>
                    <p class="mb-0"><a href="/contact.html" class="btn btn-primary rounded-0">Наші контакти</a></p>
                </div>

            </div>
        </div>



        <div class="owl-carousel owl-1 ">
            <div class="ftco-cover-1" style="background-image: url('images/hero_1.jpg');"></div>
            <div class="ftco-cover-1" style="background-image: url('images/hero_2.jpg');"></div>
            <div class="ftco-cover-1" style="background-image: url('images/hero_3.jpg');"></div>

        </div>
    </div>


    <div class="site-section">
        <div class="container">
            <div class="row align-items-stretch">
                <div class="col-lg-4">
                    <div class="h-100 bg-white box-29291">
                        <h2 class="heading-39291">Ласкаво просимо <br> До Нашої Компанії</h2>
                        <p>”Дизайн-Проект” - компанія, яка успішно функціонує вже більше 15 років у сфері будівництва та надає повний комплекс високоякісних будівельних робіт
                            - від створення проектів до будівництва об’єктів у Львові та області. Маємо усі необхідні ліцензії на виконання будівельних робіт,
                            та працюємо у строгій відповідності з проектом і ДБН. Є платниками податків на загальних засадах. Наша компанія співпрацює
                            з багатьома бізнес-партнерами - постачальники будівельних матеріалів, інструментів, техніки, обладнання.
                            Саме це дає змогу уникати посередницьких націнок та здешевлювати будівельні роботи у Житомирі раціональним способом.</p>


                        <p class="mt-5">
                            <span class="d-block font-weight-bold text-black">КИРИЛО ЛАНТВОЙТ</span>
                            <span class="d-block font-weight-bold text-muted">Засновник, Керівник компанії</span>

                            <img src="images/signature.svg" alt="Image" class="img-fluid" width="140">
                        </p>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="owl-carousel owl-3">
                        <img src="images/about_1.jpg" alt="Image" class="img-fluid">
                        <img src="images/about_2.jpg" alt="Image" class="img-fluid">
                        <img src="images/about_3.jpg" alt="Image" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="site-section">
        <div class="container">
            <div class="row mb-5 align-items-center">
                <div class="col-md-7">
                    <h2 class="heading-39291 mb-0">Що ми робимо</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="">
                    <div class="service-29193 text-center">
              <span class="img-wrap mb-5">
                <img src="fonts/flaticon/svg/001-stairs.svg" alt="Image" class="img-fluid">
              </span>
                        <h3 class="mb-4"><a href="/">Творчі сходи</a></h3>
                        <p>Дерев’яні сходи – вічна класика. Вони поширені в будинках і котеджах,
                            в готелях, ресторанах, на базах відпочинку та в будь-яких громадських місцях.</p>
                    </div>
                </div>
                <div class="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="100">
                    <div class="service-29193 text-center">
              <span class="img-wrap mb-5">
                <img src="fonts/flaticon/svg/002-kitchen.svg" alt="Image" class="img-fluid">
              </span>
                        <h3 class="mb-4"><a href="/">Дизайн кухні</a></h3>
                        <p>Кухня для багатьох є улюбленим місцем у будинку,
                            де можна створювати вишукані кулінарні шедеври,
                            дегустувати їх або просто проводити час у колі близьких людей.</p>
                    </div>
                </div>
                <div class="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="service-29193 text-center">
              <span class="img-wrap mb-5">
                <img src="fonts/flaticon/svg/003-lamp.svg" alt="Image" class="img-fluid">
              </span>
                        <h3 class="mb-4"><a href="/">Декоративні світильники</a></h3>
                        <p>Декоративний світильник - це освітлювальний прилад,
                            який використовується як прикраса приміщення,
                            створює настрій, може стати чудовим подарунком, а також зразком інтер'єру. </p>
                    </div>
                </div>

                <div class="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="">
                    <div class="service-29193 text-center">
              <span class="img-wrap mb-5">
                <img src="fonts/flaticon/svg/004-blueprint.svg" alt="Image" class="img-fluid">
              </span>
                        <h3 class="mb-4"><a href="/">План інтер'єру</a></h3>
                        <p>Дизайн-проект інтер’єру, розроблений нашими фахівцями, буде включати повний пакет документів,
                            а також тривимірні моделі майбутнього дизайну приміщення.</p>
                    </div>
                </div>
                <div class="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="100">
                    <div class="service-29193 text-center">
              <span class="img-wrap mb-5">
                <img src="fonts/flaticon/svg/005-dinning-table.svg" alt="Image" class="img-fluid">
              </span>
                        <h3 class="mb-4"><a href="/">Обідній стіл</a></h3>
                        <p>Обідні столи потрібні на всіх кухнях та вітальнях.
                            За столом всі збираються для спілкування, чи смачної вечері.</p>
                    </div>
                </div>
                <div class="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="service-29193 text-center">
              <span class="img-wrap mb-5">
                <img src="fonts/flaticon/svg/006-pantone.svg" alt="Image" class="img-fluid">
              </span>
                        <h3 class="mb-4"><a href="/">Сучасний дизайн</a></h3>
                        <p>Інтер'єр будинку поєднує комфорт і витонченість,
                            зберігаючи чітку стилістику оформлення в кожному приміщенні,
                            грамотно балансуючи між стриманістю і невимушеністю. .</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section">
        <div class="container">
            <div class="row mb-5 align-items-center">
                <div class="col-md-7">
                    <h2 class="heading-39291 mb-0">Наші проекти</h2>
                </div>
                <div class="col-md-5 text-right">
                    <p class="mb-0"><a href="/project.html" class="more-39291">Переглянути всі наші проекти</a></p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="media-02819">
                        <a href="/" class="img-link small"><img src="images/img_1.jpg" alt="Image" class="img-fluid"></a>
                        <h3><a href="/project.html">Дизайн-проект Полежавская</a></h3>
                        <span>Житомир, Україна</span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="media-02819">
                        <a href="/" class="img-link"><img src="images/img_2.jpg" alt="Image" class="img-fluid"></a>
                        <h3><a href="/project.html">Дизайн-проект Кухні</a></h3>
                        <span>Житомир, Україна</span>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="media-02819">
                        <a href="/" class="img-link"><img src="images/img_3.jpg" alt="Image" class="img-fluid"></a>
                        <h3><a href="/project.html">Дизайн-проект Вітальні</a></h3>
                        <span>Житомир, Україна</span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="media-02819">
                        <a href="/" class="img-link small"><img src="images/img_4.jpg" alt="Image" class="img-fluid"></a>
                        <h3><a href="/project.html">Дизайн-проект Мінімалізм</a></h3>
                        <span>Житомир, Україна</span>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="site-section section-4">
        <div class="container">

            <div class="row justify-content-center text-center">
                <div class="col-md-7">
                    <div class="slide-one-item owl-carousel">
                        <blockquote class="testimonial-1">
                            <span class="quote quote-icon-wrap"><span class="icon-format_quote"></span></span>
                            <p>Головне правило у житті – слідувати своїм мріям… Незалежно від того, що ти робиш, ти маєш йти за своїми мріями!</p>
                            <cite><span class="text-black">КИРИЛО</span> &mdash; <span class="text-muted">Засновник</span></cite>
                        </blockquote>

                        <blockquote class="testimonial-1">
                            <span class="quote quote-icon-wrap"><span class="icon-format_quote"></span></span>
                            <p>Мріяти корисно і чудово, адже тільки з мрії можна виділити мету в житті і йти до неї.</p>
                            <cite><span class="text-black">ВІКТОРІЯ</span> &mdash; <span class="text-muted">Дизайнер інтер'єру</span></cite>
                        </blockquote>

                        <blockquote class="testimonial-1">
                            <span class="quote quote-icon-wrap"><span class="icon-format_quote"></span></span>
                            <p> Мрії надихають і стимулюють, змушують дивитися в майбутнє, не прив'язуючись до минулого. Але... самі по собі мрії і бажання цілями не стають. </p>
                            <cite><span class="text-black">ОЛЬГА</span> &mdash; <span class="text-muted">Дизайнер інтер'єру</span></cite>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="site-section">
        <div class="container">
            <div class="row  mb-5">
                <div class="col-md-7">
                    <h2 class="heading-39291">Блог та новини</h2>
                    <p>Віртуальні тури по будинках, характерні деталі, незвичайні поєднання кольорів
                         ... Хто з вас любить розглядати фотографії інтер'єрів? Ми зробили добірку десяти надихаючих блогів про дизайн інтер'єру,
                        які будуть корисні тим, хто планує ремонт або просто цікавиться дизайном.</p>

                </div>
            </div>
            <div class="post-entry-1-contents">
                <span class="meta">26/08/2021</span>
                <h2><a href="/blog1.html">Комплектація кухні технікою</a></h2>
                <p class="my-3"><a href="/blog1.html" class="more-39291">Читати далі</a></p>
            </div>
        </div>
    </div>
                <div class="col-lg-3 col-md-6 mb-5">
                    <div class="post-entry-1 h-100">

                        <div class="post-entry-1-contents">
                            <span class="meta">19/08/2021</span>
                            <h2><a href="#">Стилі інтер’єру</a></h2>
                            <p class="my-3"><a href="#" class="more-39291">Читати далі</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-5">
                    <div class="post-entry-1 h-100">

                        <div class="post-entry-1-contents">
                            <span class="meta">04/08/2021</span>
                            <h2><a href="#">Хто такий дизайнер інтер’єр</a></h2>
                            <p class="my-3"><a href="#" class="more-39291">Читати далі</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-5">
                    <div class="post-entry-1 h-100">

                        <div class="post-entry-1-contents">
                            <span class="meta">05/08/2021</span>
                            <h2><a href="#">«Смачний» дизайн інтер’єру магазину солодощів у Львові</a></h2>
                            <p class="my-3"><a href="#" class="more-39291">Читати далі</a></p>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="google">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2551.523964949049!2d28.63521091572404!3d50.24479757944744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x472c6493ebf252ed%3A0xc400e72454e33b55!2z0JTQtdGA0LbQsNCy0L3QuNC5INGD0L3RltCy0LXRgNGB0LjRgtC10YIgwqvQltC40YLQvtC80LjRgNGB0YzQutCwINC_0L7Qu9GW0YLQtdGF0L3RltC60LDCuw!5e0!3m2!1sru!2sua!4v1642526267044!5m2!1sru!2sua"
                width="100%" height="560" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-7">
                            <h2 class="footer-heading mb-4">Про Нас</h2>
                            <p>Це молода група професійних архітекторів, які знають та з відповідальністю відносяться до своєї справи.
                                Проектуємо та співпрацюємо по всій Україні з 20022 року. </p>

                        </div>
                        <div class="col-md-4 ml-auto">
                            <h2 class="footer-heading mb-4">Особливості</h2>
                            <ul class="list-unstyled">
                                <li><a href="/about.html">Про Нас</a></li>
                                <li><a href="/blog1.html">Відгуки</a></li>
                                <li><a href="/blog1.html">Умови обслуговування</a></li>
                                <li><a href="/blog1.html">Конфіденційність</a></li>
                                <li><a href="/contact.html">Наші контакти</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-md-4 ml-auto">

                    <div class="mb-5">
                        <h2 class="footer-heading mb-4">Підпишіться  на Новини</h2>
                        <form action="/blog.html" method="post" class="footer-suscribe-form">
                            <div class="input-group mb-3">
                                <div class="input-group-append"> <form class="d-flex">
                                        <? if(!$userModel->IsUserAuthenticated()) : ?>
                                            <a href="/users/register" class="btn btn-primary text-white" type="button">Реєстрація</a>
                                            <a href="/users/login" class="btn btn-primary text-white" type="button">Увійти</a>
                                        <? else: ?>
                                            <span class="nav-link"><?=$user['login'] ?></span>
                                            <a href="/users/logout" class="btn btn-primary">Вийти</a>
                                        <? endif; ?>
                                    </form>
                                </div>
                                <div class= "container">
                                    <h1 class= "mt-5"><?=$PageTitle ?></h1>
                                    <? if(!empty($MessageText)):?>
                                        <div class="alert alert-<?=$MessageClass ?>" role="alert">
                                            <?=$MessageText ?>
                                        </div>
                                    <? endif; ?>
                                    <? ?>
                                    <?=$PageContent ?>
                                </div>
                            </div>
                    </div>

                    <h2 class="footer-heading mb-4">Підпишіться </h2>
                    <a href="/about-section" class="smoothscroll pl-0 pr-3"><span class="icon-facebook"></span></a>
                    <a href="/" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                    <a href="/" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                    <a href="/" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                    </form>
                </div>
            </div>
            <div class="row pt-5 mt-5 text-center">
                <div class="col-md-12">
                    <div class="pt-5">
                        <p class="small">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made by Voron <i class="icon-heart text-danger" aria-hidden="true"></i>  <a href="https://colorlib.com" target="_blank" ></a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </footer>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/aos.js"></script>

<script src="js/main.js"></script>


</body>

</html>


