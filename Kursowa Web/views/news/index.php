<?php
$userModel = new \models\Users();
$user = $userModel->GetCurrentUser();
?>
<?php foreach ($lastNews as $news) : ?>
<div class="news-record">
    <h3><?= $news['title'] ?></h3>
    <div class="photo">
        <? if (is_file('files/news/'.$news['photo'].'_s.jpg')) : ?>
            <img class="bd-placeholder-img rounded float-start"  src="/files/news/<?=$news['photo']?>_s.jpg"/>
        <? else : ?>
        <svg class="bd-placeholder-img rounded float-start" width="200" height="200" xmlns="http://www.w3.org/2000/svg" role="img" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect></svg>
        <? endif; ?>
    </div>
    <div>
        <h3><?=$news['short_text'] ?></h3>
    </div>
    <div>
        <a href="/news/view?id=<?=$news['id'] ?>" class="btn btn-primary">Читати далі</a>
        <? if($news['user_id'] == $user['id']) :  ?>
        <a href="/news/edit?id=<?=$news['id'] ?>"class="btn btn-success">Редагування</a>
        <a href="/news/delete?id=<?=$news['id'] ?>"class="btn btn-danger">Видалити</a>
        <? endif; ?>
    </div>
</div>
<?php endforeach; ?>

